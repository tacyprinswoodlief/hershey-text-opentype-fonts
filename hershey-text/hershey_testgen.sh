#!/bin/bash
# Run in extensions directory to generate test references for Hershey Text.

# Basic test:
python3 hershey.py tests/data/svg/hershey_input.svg > tests/data/refs/hershey.out



# loading a different font:
python3 hershey.py --fontface="EMSAllure" \
    tests/data/svg/hershey_trivial_input.svg > tests/data/refs/hershey_loadfont.out

# test preserve text option:
python3 hershey.py --fontface="EMSOsmotron" --preserve="True"\
    tests/data/svg/hershey_trivial_input.svg > tests/data/refs/hershey_preservetext.out
    
# test when just part of the input file is selected:
python3 hershey.py --id=A\
    tests/data/svg/hershey_trivial_input.svg > tests/data/refs/hershey_partialselection.out



# test font table generation:
python3 hershey.py --tab="utilities" --action="sample" --text="I am a quick brown fox"\
    tests/data/svg/default-inkscape-SVG.svg > tests/data/refs/hershey_fonttable.out

# test proper handling of UTF-8 input text:
python3 hershey.py --tab="utilities" --action="sample" --text="Î âm å qù¡çk brõwñ fø×"\
    tests/data/svg/default-inkscape-SVG.svg > tests/data/refs/hershey_encoding.out

# test glyph table generation:
python3 hershey.py --tab="utilities" --action="table" --fontface="other" --otherfont="EMSOsmotron"\
    tests/data/svg/default-inkscape-SVG.svg > tests/data/refs/hershey_glyphtable.out
